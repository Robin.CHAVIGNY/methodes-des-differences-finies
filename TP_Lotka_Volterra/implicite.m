function [Xnew]=implicite(X,h,alpha,beta,delta,gamma)

k=0;
flag=0;
Xnew= X;
eps=10^(-6);

while (flag==0)
    A=zeros(2);
    A(1,1)=1-h*alpha;
    A(1,2)=h*beta*Xnew(1);
    A(2,1)=-h*delta*Xnew(2);
    A(2,2)=1+h*gamma;
    
    F=[Xnew(1)-h*Xnew(1)*(alpha-beta*Xnew(2))-X(1)
        Xnew(2)-h*Xnew(2)*(delta*Xnew(1)-gamma)-X(2)];
    
    U=A\-F;
    
    Xnew=Xnew+U;
    
    if (norm(U)/norm(Xnew) > eps)
        k=k+1;
    else
        flag=1;
    end 
end
