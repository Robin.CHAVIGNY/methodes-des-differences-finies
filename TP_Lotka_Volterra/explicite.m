function [Xnew]=explicite(X,h,alpha,beta,delta,gamma)

Xnew=[0;0];
Xnew(1)=X(1) * (1 + h*(alpha - beta * X(2)));
Xnew(2)=X(2) * (1 + h*(delta*X(1) - gamma));