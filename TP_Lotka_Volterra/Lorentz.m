function [] = Lorentz()
    h = 1/10000;
    t = 0:h:50;
    n = length(t);

    X = [1; 1; 1];
    Xt = zeros(3, n);
    Xt(:, 1) = X;

    % Création de la figure en dehors de la boucle
    figure;
    xlabel('x(t)');
    ylabel('y(t)');
    zlabel('z(t)');
    title('Modèle de Lorenz - Courbe 3D des composantes de Xt');
    hold on;
    plot3(Xt(1, 1), Xt(2, 1), Xt(3, 1), 'r.'); % Tracé initial (point rouge)
    
    for i = 2:n
        % Exemple avec des valeurs aléatoires à la place de impliLo
        X = impliLo(X,h); % Remplacez cette ligne par votre fonction impliLo

        Xt(:, i) = X;

        % Mise à jour du tracé dans la même figure
        plot3(Xt(1, 1:i), Xt(2, 1:i), Xt(3, 1:i), 'b'); % Tracé mis à jour (ligne bleue)
        grid on;
        %axis equal;
        drawnow; % Met à jour l'affichage de la figure
        

        %pause(0.00001); % Attente courte pour mieux visualiser l'évolution
    end
end

