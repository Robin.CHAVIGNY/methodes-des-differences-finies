function [Xnew]=impliLo(X,h)

k=0;
flag=0;
Xnew= X;
eps=10^(-6);
itmax = 100;



while (flag==0 && k<itmax)
    l=Xnew(1)-h*10*(Xnew(2)-Xnew(1)) -X(1);
    l2=Xnew(2) -h*(28*Xnew(1) -Xnew(2) - Xnew(1)*Xnew(2)) -X(2);
    l3=Xnew(3) -h*(Xnew(1)*Xnew(2)-(8/3)*Xnew(3)) -X(3);
    F=[l;l2;l3];
 
    DF=zeros(3);
    DF(1,1)=1+10*h;
    DF(1,2)=-10*h;
    DF(1,3)= 0;
    DF(2,1)=-h*28+h*Xnew(3);
    DF(2,2)=1+h;
    DF(2,3)=h*Xnew(1);
    DF(3,1)=-h*Xnew(2);
    DF(3,2)=-h*Xnew(1);
    DF(3,3)=1+h*8/3;
    
    U=DF\-F;
    
    Xnew=Xnew+U;
    
    if (norm(U)/norm(Xnew) > eps)
        k=k+1;
    else
        flag=1;
    end 
end