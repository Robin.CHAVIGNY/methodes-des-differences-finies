function []=lapin_renard()

alpha=0.05;
beta=5*10^(-4);
delta=5*10^(-3);
gamma=0.2;

h=1/10;

t=0:h:600;

x1=[600;100];
x2=[1000;100];
x3=[1500;100];



xt=[x3];
n=length(t);

for i=2:n
    xt=[xt,explicite(xt(:,i-1),h,alpha,beta,delta,gamma)];
    %xt=[xt,semi_implicite(xt(:,i-1),h,alpha,beta,delta,gamma)];
    %xt=[xt,implicite(xt(:,i-1),h,alpha,beta,delta,gamma)];
end
%r=lapin 
plot(t,xt(1,:),"red",t,xt(2,:),"blue",xt(1,:),xt(2,:),"green")
