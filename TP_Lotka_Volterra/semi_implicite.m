function [Xnew]=semi_implicite(X,h,alpha,beta,delta,gamma)

Xnew=[0;0];
A=zeros(2);
A(1,1)=1-h*alpha;
A(1,2)=h*beta*X(1);
A(2,1)=-h*delta*X(2);
A(2,2)=1+h*gamma;
Xnew = A\X;
